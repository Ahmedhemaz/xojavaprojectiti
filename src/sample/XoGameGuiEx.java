package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class XoGameGuiEx extends Application {

    public XoGameGuiEx(){
        /*
        * XoGameGuiEx default constructor
        * */
    }

    private FirstBase root;
    private XoGameController xoGameController;

    @Override
    public  void start(Stage primaryStage) throws Exception{
        primaryStage.setResizable(false);
        
        root = new FirstBase(); // create new XoGameGui from gui class of scene builder
        /*
        * create xoGameController instance and pass reference of the gui instance to it check -> xoGameController
        * */
        xoGameController = new XoGameController(root);
        Scene scene = new Scene(root);
        primaryStage.setTitle("XO Game");
        primaryStage.setScene(scene);
       // primaryStage.setResizable(false);
        primaryStage.show();
    }



    public static void main(String[] args) {
        launch(args);
    }


    public XoGameController getXoGameController() {
        System.out.println("getXoGameController Here");
        return xoGameController;
    }
}
