package sample;

import java.io.Serializable;

public class XoGameNetworkData implements Serializable {

    volatile private int index;
    volatile private boolean player1Turn;
    volatile private boolean player2Turn;
    volatile private boolean canPlay;
    private boolean inGame;
    private String name;
    private StartSessionFlag startSessionFlag;

    public XoGameNetworkData() {
        this.player1Turn = true;
        this.player2Turn = false;
        this.canPlay = true;
        this.inGame = false;
        this.startSessionFlag = new StartSessionFlag();
    }


    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean isPlayer1Turn() {
        return player1Turn;
    }

    public void setPlayer1Turn(boolean player1Turn) {
        this.player1Turn = player1Turn;
    }

    public boolean isPlayer2Turn() {
        return player2Turn;
    }

    public void setPlayer2Turn(boolean player2Turn) {
        this.player2Turn = player2Turn;
    }

    public boolean isInGame() {
        return inGame;
    }

    public void setInGame(boolean inGame) {
        this.inGame = inGame;
    }


    public boolean isCanPlay() {
        return canPlay;
    }

    public void setCanPlay(boolean canPlay) {
        this.canPlay = canPlay;
    }

    class StartSessionFlag implements Serializable{
        private boolean startSessionFlag;

        public StartSessionFlag() {
            System.out.println("start flag constructor");
            this.startSessionFlag = true;
        }

        public boolean isStartSessionFlag() {
            return startSessionFlag;
        }

        public void setStartSessionFlag(boolean startSessionFlag) {
            this.startSessionFlag = startSessionFlag;
        }
    }

    public Boolean getStartSessionFlag() {
        System.out.println("start flag boolean inner class" + startSessionFlag.isStartSessionFlag());
        return startSessionFlag.isStartSessionFlag();
    }

    public void setStartSessionFlag(Boolean sessionFlag) {
        this.startSessionFlag.setStartSessionFlag(sessionFlag);
    }
}