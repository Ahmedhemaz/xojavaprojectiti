package sample;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class BonusClass extends AnchorPane {

    protected final ImageView imageView;
    protected final Text WinnerText;
    protected final Button ExitBtn;

    public BonusClass() {

        imageView = new ImageView();
        WinnerText = new Text();
        ExitBtn = new Button();

        setId("AnchorPane");
        setMaxHeight(553.0);
        setMaxWidth(469.0);
        setPrefHeight(553.0);
        setPrefWidth(469.0);

        imageView.setFitHeight(553.0);
        imageView.setFitWidth(469.0);
       imageView.setImage(new Image(getClass().getResource("lastImg.jpg").toExternalForm()));
       
       
        WinnerText.setFill(javafx.scene.paint.Color.WHITE);
        WinnerText.setLayoutX(130.0);
        WinnerText.setLayoutY(66.0);
        WinnerText.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        WinnerText.setStrokeWidth(0.0);
        WinnerText.setWrappingWidth(262.13671875);
        WinnerText.setFont(new Font("System Bold", 30.0));

//        WinnerText.setFill(javafx.scene.paint.Color.WHITE);
//        WinnerText.setLayoutX(161.0);
//        WinnerText.setLayoutY(68.0);
//        WinnerText.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
//        WinnerText.setStrokeWidth(0.0);
//        WinnerText.setWrappingWidth(152.13671875);
//        WinnerText.setFont(new Font("System Bold", 18.0));

        ExitBtn.setLayoutX(155.0);
        ExitBtn.setLayoutY(500.0);
        ExitBtn.setMnemonicParsing(false);
        ExitBtn.setPrefHeight(34.0);
        ExitBtn.setPrefWidth(123.0);
        ExitBtn.setStyle("-fx-background-color: transparent; -fx-border-color: #bf8383; -fx-border-width: 1.5px; -fx-border-radius: 15px; -fx-min-width: 160px;");
        ExitBtn.setText("OK");
        ExitBtn.setTextFill(javafx.scene.paint.Color.valueOf("#bf8383"));
        ExitBtn.setFont(new Font("System Bold", 18.0));

        getChildren().add(imageView);
        getChildren().add(WinnerText);
        getChildren().add(ExitBtn);

    }
}
