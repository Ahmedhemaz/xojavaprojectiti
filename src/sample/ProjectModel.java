package sample;

public class ProjectModel {

    protected AI aiPlayer;
    protected HumanPlayer humanPlayer;
    protected HumanPlayer humanPlayer2;
    protected Game game;
    protected NetworkConnection networkConnection;

    public ProjectModel(){}

    public AI getAiPlayer() {
        return aiPlayer;
    }

    public void setAiPlayer(char gameToken) {
        if (gameToken == 'X'){
            this.aiPlayer = new AI(HumanPlayer.Type.X);
        }
        if (gameToken == 'O'){
            this.aiPlayer = new AI(HumanPlayer.Type.O);
        }

    }

    public HumanPlayer getHumanPlayer1() {
        return humanPlayer;
    }

    public void setHumanPlayer1(char gameToken) {
        if(gameToken == 'X'){
            this.humanPlayer = new HumanPlayer(HumanPlayer.Type.X);
        }
        if(gameToken == 'O'){
            this.humanPlayer = new HumanPlayer(HumanPlayer.Type.O);
        }
    }

    public void setHumanPlayer2(char gameToken) {
        if(gameToken == 'X'){
            this.humanPlayer = new HumanPlayer(HumanPlayer.Type.X);
        }
        if(gameToken == 'O'){
            this.humanPlayer = new HumanPlayer(HumanPlayer.Type.O);
        }
    }

    public HumanPlayer getHumanPlayer2() {
        return humanPlayer2;
    }

    public Game getGame() {
        return game;
    }

    public void createGame() {
        this.game = new Game();
    }

    public NetworkConnection getNetworkConnection() {
        return networkConnection;
    }

    public void setNetworkConnection(String ip, int socket) {
        this.networkConnection = new NetworkConnection(ip,socket);
    }
}
