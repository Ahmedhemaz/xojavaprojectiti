package sample;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import static javafx.scene.layout.Region.USE_PREF_SIZE;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class FirstBase extends BorderPane {

    protected final BorderPane WholeGreatPane;
    protected final Pane pane;
    protected final Pane pane0;
    protected final Text text;
    protected final Button playBtn;
    protected final Reflection reflection;
    protected final Button networkBtn;
    protected final Reflection reflection0;
    protected final Text text0;
    protected final Text text1;

    public FirstBase() {

        WholeGreatPane = new BorderPane();
        pane = new Pane();
        pane0 = new Pane();
        text = new Text();
        playBtn = new Button();
        reflection = new Reflection();
        networkBtn = new Button();
        reflection0 = new Reflection();
        text0 = new Text();
        text1 = new Text();


        pane.setMaxHeight(USE_PREF_SIZE);
        pane.setMaxWidth(USE_PREF_SIZE);
        pane.setMinHeight(USE_PREF_SIZE);
        pane.setMinWidth(USE_PREF_SIZE);
        pane.setPrefHeight(553.0);
        pane.setPrefWidth(469.0);
        pane.setStyle("-fx-background-color: #080d21;");

        pane0.setLayoutX(51.0);
        pane0.setLayoutY(43.0);
        pane0.setPrefHeight(473.0);
        pane0.setPrefWidth(371.0);
        pane0.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(94.0);
        text.setLayoutY(81.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold Italic", 34.0));

        playBtn.setLayoutX(52.0);
        playBtn.setLayoutY(148.0);
        playBtn.setMnemonicParsing(false);
        playBtn.setPrefHeight(69.0);
        playBtn.setPrefWidth(264.0);
        playBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px; -fx-border-radius: 20px;");
        playBtn.getStyleClass().add("btn");
        playBtn.setText("Play Game");
        playBtn.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        playBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        playBtn.setCursor(Cursor.HAND);

        playBtn.setEffect(reflection);

        networkBtn.setLayoutX(49.0);
        networkBtn.setLayoutY(311.0);
        networkBtn.setMnemonicParsing(false);
        networkBtn.setPrefHeight(69.0);
        networkBtn.setPrefWidth(264.0);
        networkBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #e29cd8; -fx-border-style: solid; -fx-border-width: 4px;");
        networkBtn.getStyleClass().add("btn");
        networkBtn.setText("Play Over Network");
        networkBtn.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        networkBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        networkBtn.setCursor(Cursor.HAND);

        networkBtn.setEffect(reflection0);

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(153.0);
        text0.setLayoutY(81.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(60.13671875);
        text0.setFont(new Font("System Bold Italic", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(219.0);
        text1.setLayoutY(82.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(60.13671875);
        text1.setFont(new Font("System Bold Italic", 34.0));
        WholeGreatPane.setCenter(pane);
        setCenter(WholeGreatPane);

        pane0.getChildren().add(text);
        pane0.getChildren().add(playBtn);
        pane0.getChildren().add(networkBtn);
        pane0.getChildren().add(text0);
        pane0.getChildren().add(text1);
        pane.getChildren().add(pane0);

    }
}


/**
 * 
 * 
 * 
 * 
 * playBtn.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event) {
         //System.out.println("About");
         SecondBase scecondsceneParent=new SecondBase();
         Scene scecondscene = new Scene(scecondsceneParent);
         Stage widow=(Stage)((Node)event.getSource()).getScene().getWindow();
        
         widow.setScene(scecondscene);
         widow.show();
        }
        });
 
 * 
 * 
 * 
 */