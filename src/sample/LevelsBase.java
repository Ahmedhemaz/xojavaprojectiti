package sample;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class LevelsBase extends Pane {

    protected final Pane pane;
    protected final Text text;
    protected final Button meduimBtn;
    protected final Button hardBtn;
    protected final Text text0;
    protected final Text text1;
    protected final Button simpleBtn;

    public LevelsBase() {

        pane = new Pane();
        text = new Text();
        meduimBtn = new Button();
        hardBtn = new Button();
        text0 = new Text();
        text1 = new Text();
        simpleBtn = new Button();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(553.0);
        setPrefWidth(469.0);
        setStyle("-fx-background-color: #080d21;");

        pane.setLayoutX(51.0);
        pane.setLayoutY(43.0);
        pane.setPrefHeight(473.0);
        pane.setPrefWidth(371.0);
        pane.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(89.0);
        text.setLayoutY(88.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold", 34.0));

        meduimBtn.setLayoutX(52.0);
        meduimBtn.setLayoutY(241.0);
        meduimBtn.setMnemonicParsing(false);
        meduimBtn.setPrefHeight(69.0);
        meduimBtn.setPrefWidth(264.0);
        meduimBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        meduimBtn.getStyleClass().add("btn");
        meduimBtn.setText("Meduim");
        meduimBtn.setTextFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        meduimBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        meduimBtn.setCursor(Cursor.HAND);
        meduimBtn.setId("M");

        hardBtn.setLayoutX(51.0);
        hardBtn.setLayoutY(341.0);
        hardBtn.setMnemonicParsing(false);
        hardBtn.setPrefHeight(69.0);
        hardBtn.setPrefWidth(264.0);
        hardBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        hardBtn.getStyleClass().add("btn");
        hardBtn.setText("Hard");
        hardBtn.setId("H");
        hardBtn.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        hardBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        hardBtn.setCursor(Cursor.HAND);

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(152.0);
        text0.setLayoutY(86.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(63.13671875);
        text0.setFont(new Font("System Bold", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(227.0);
        text1.setLayoutY(85.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(73.13671875);
        text1.setFont(new Font("System Bold", 34.0));

        simpleBtn.setLayoutX(49.0);
        simpleBtn.setLayoutY(140.0);
        simpleBtn.setMnemonicParsing(false);
        simpleBtn.setPrefHeight(69.0);
        simpleBtn.setPrefWidth(264.0);
        simpleBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        simpleBtn.getStyleClass().add("btn");
        simpleBtn.setText("Simple");
        simpleBtn.setId("S");
        simpleBtn.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        simpleBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        simpleBtn.setCursor(Cursor.HAND);

        pane.getChildren().add(text);
        pane.getChildren().add(meduimBtn);
        pane.getChildren().add(hardBtn);
        pane.getChildren().add(text0);
        pane.getChildren().add(text1);
        pane.getChildren().add(simpleBtn);
        getChildren().add(pane);

    }
}
