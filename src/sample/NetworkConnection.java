package sample;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.io.*;
import java.net.Socket;
import java.util.Optional;

public class NetworkConnection {

    Socket mySocket;
    ObjectInputStream objectInputStream ;
    ObjectOutputStream objectOutputStream;


    public NetworkConnection(String ip, int portNumber) {

        try{

            this.mySocket = new Socket(ip, portNumber);
            this.objectOutputStream = new ObjectOutputStream(mySocket.getOutputStream());
            this.objectInputStream = new ObjectInputStream(mySocket.getInputStream());

        } catch(IOException ex) {

            System.out.println("Server is down");
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Dialog");
            alert.setHeaderText("Connection Error");
            alert.setContentText("Server Is Down Try Again Later");
            ButtonType buttonTypeOne = new ButtonType("Exit");
            alert.getButtonTypes().setAll(buttonTypeOne);
            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == buttonTypeOne){
                System.exit(0);
            }
            //ex.printStackTrace();
        }

    }

    public ObjectInputStream getObjectInputStream() {
        return objectInputStream;
    }

    public ObjectOutputStream getObjectOutputStream() {
        return objectOutputStream;
    }

}
