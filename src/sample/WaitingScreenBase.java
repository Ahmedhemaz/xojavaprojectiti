package sample;

import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class WaitingScreenBase extends BorderPane {

    protected final BorderPane WholeGreatPane;
    protected final Pane pane;
    protected final Pane pane0;
    protected final Text text;
    protected final Text text0;
    protected final Text text1;
    protected final Text text2;

    public WaitingScreenBase() {

        WholeGreatPane = new BorderPane();
        pane = new Pane();
        pane0 = new Pane();
        text = new Text();
        text0 = new Text();
        text1 = new Text();
        text2 = new Text();


        pane.setMaxHeight(USE_PREF_SIZE);
        pane.setMaxWidth(USE_PREF_SIZE);
        pane.setMinHeight(USE_PREF_SIZE);
        pane.setMinWidth(USE_PREF_SIZE);
        pane.setPrefHeight(553.0);
        pane.setPrefWidth(469.0);
        pane.setStyle("-fx-background-color: #080d21;");

        pane0.setLayoutX(51.0);
        pane0.setLayoutY(43.0);
        pane0.setPrefHeight(473.0);
        pane0.setPrefWidth(371.0);
        pane0.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(94.0);
        text.setLayoutY(81.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold Italic", 34.0));

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(153.0);
        text0.setLayoutY(81.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(60.13671875);
        text0.setFont(new Font("System Bold Italic", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(219.0);
        text1.setLayoutY(82.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(60.13671875);
        text1.setFont(new Font("System Bold Italic", 34.0));

        text2.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text2.setLayoutX(82.0);
        text2.setLayoutY(220.0);
        text2.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text2.setStrokeWidth(0.0);
        text2.setText("No one Active Now !");
        text2.setWrappingWidth(241.13671875);
        text2.setFont(new Font("System Bold Italic", 24.0));
        WholeGreatPane.setCenter(pane);
        setCenter(WholeGreatPane);

        pane0.getChildren().add(text);
        pane0.getChildren().add(text0);
        pane0.getChildren().add(text1);
        pane0.getChildren().add(text2);
        pane.getChildren().add(pane0);

    }
}
