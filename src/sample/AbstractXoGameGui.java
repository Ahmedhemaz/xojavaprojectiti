package sample;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

public abstract class AbstractXoGameGui extends GridPane {

    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final ColumnConstraints columnConstraints1;
    protected final RowConstraints rowConstraints;
    protected final RowConstraints rowConstraints0;
    protected final RowConstraints rowConstraints1;
    protected final Button button0;
    protected final ImageView imageView;
    protected final Button button1;
    protected final ImageView image1;
    protected final Button button2;
    protected final ImageView image2;
    protected final Button button3;
    protected final ImageView image3;
    protected final Button button4;
    protected final ImageView image4;
    protected final Button button5;
    protected final ImageView image5;
    protected final Button button6;
    protected final ImageView image6;
    protected final Button button7;
    protected final ImageView image7;
    protected final Button button8;
    protected final ImageView image8;

    public AbstractXoGameGui() {

        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        columnConstraints1 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        rowConstraints0 = new RowConstraints();
        rowConstraints1 = new RowConstraints();
        button0 = new Button();
        imageView = new ImageView();
        button1 = new Button();
        image1 = new ImageView();
        button2 = new Button();
        image2 = new ImageView();
        button3 = new Button();
        image3 = new ImageView();
        button4 = new Button();
        image4 = new ImageView();
        button5 = new Button();
        image5 = new ImageView();
        button6 = new Button();
        image6 = new ImageView();
        button7 = new Button();
        image7 = new ImageView();
        button8 = new Button();
        image8 = new ImageView();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(480.0);
        setPrefWidth(640.0);


        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        columnConstraints1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints0.setMinHeight(10.0);
        rowConstraints0.setPrefHeight(30.0);
        rowConstraints0.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints1.setMinHeight(10.0);
        rowConstraints1.setPrefHeight(30.0);
        rowConstraints1.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        button0.setMnemonicParsing(false);
        button0.setPrefHeight(168.0);
        button0.setPrefWidth(212.0);
        //button0.setStyle("-fx-background-image: 182;");

        imageView.setFitHeight(150.0);
        imageView.setFitWidth(150.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        imageView.setStyle("-fx-opacity: 1;");
        imageView.setImage(new Image(getClass().getResource("X.png").toExternalForm()));
        button0.setGraphic(imageView);

        GridPane.setColumnIndex(button1, 1);
        button1.setMnemonicParsing(false);
        button1.setPrefHeight(191.0);
        button1.setPrefWidth(271.0);

        image1.setFitHeight(150.0);
        image1.setFitWidth(150.0);
        image1.setPickOnBounds(true);
        image1.setPreserveRatio(true);
        image1.setImage(new Image(getClass().getResource("O.png").toExternalForm()));
        button1.setGraphic(image1);

        GridPane.setColumnIndex(button2, 2);
        button2.setMnemonicParsing(false);
        button2.setPrefHeight(201.0);
        button2.setPrefWidth(250.0);

        image2.setFitHeight(110.0);
        image2.setFitWidth(182.0);
        image2.setPickOnBounds(true);
        image2.setPreserveRatio(true);
        button2.setGraphic(image2);

        GridPane.setRowIndex(button3, 1);
        button3.setMnemonicParsing(false);
        button3.setPrefHeight(231.0);
        button3.setPrefWidth(298.0);

        image3.setFitHeight(110.0);
        image3.setFitWidth(182.0);
        image3.setPickOnBounds(true);
        image3.setPreserveRatio(true);
        button3.setGraphic(image3);

        GridPane.setColumnIndex(button4, 1);
        GridPane.setRowIndex(button4, 1);
        button4.setMnemonicParsing(false);
        button4.setPrefHeight(185.0);
        button4.setPrefWidth(289.0);

        image4.setFitHeight(110.0);
        image4.setFitWidth(182.0);
        image4.setPickOnBounds(true);
        image4.setPreserveRatio(true);
        button4.setGraphic(image4);

        GridPane.setColumnIndex(button5, 2);
        GridPane.setRowIndex(button5, 1);
        button5.setMnemonicParsing(false);
        button5.setPrefHeight(191.0);
        button5.setPrefWidth(337.0);

        image5.setFitHeight(110.0);
        image5.setFitWidth(182.0);
        image5.setPickOnBounds(true);
        image5.setPreserveRatio(true);
        button5.setGraphic(image5);

        GridPane.setRowIndex(button6, 2);
        button6.setMnemonicParsing(false);
        button6.setPrefHeight(262.0);
        button6.setPrefWidth(329.0);

        image6.setFitHeight(110.0);
        image6.setFitWidth(182.0);
        image6.setPickOnBounds(true);
        image6.setPreserveRatio(true);
        button6.setGraphic(image6);

        GridPane.setColumnIndex(button7, 1);
        GridPane.setRowIndex(button7, 2);
        button7.setMnemonicParsing(false);
        button7.setPrefHeight(257.0);
        button7.setPrefWidth(316.0);

        image7.setFitHeight(110.0);
        image7.setFitWidth(182.0);
        image7.setPickOnBounds(true);
        image7.setPreserveRatio(true);
        button7.setGraphic(image7);

        GridPane.setColumnIndex(button8, 2);
        GridPane.setRowIndex(button8, 2);
        button8.setMnemonicParsing(false);
        button8.setPrefHeight(331.0);
        button8.setPrefWidth(386.0);

        image8.setFitHeight(110.0);
        image8.setFitWidth(182.0);
        image8.setPickOnBounds(true);
        image8.setPreserveRatio(true);
        button8.setGraphic(image8);

        getColumnConstraints().add(columnConstraints);
        getColumnConstraints().add(columnConstraints0);
        getColumnConstraints().add(columnConstraints1);
        getRowConstraints().add(rowConstraints);
        getRowConstraints().add(rowConstraints0);
        getRowConstraints().add(rowConstraints1);
        getChildren().add(button0);
        getChildren().add(button1);
        getChildren().add(button2);
        getChildren().add(button3);
        getChildren().add(button4);
        getChildren().add(button5);
        getChildren().add(button6);
        getChildren().add(button7);
        getChildren().add(button8);

    }

}
