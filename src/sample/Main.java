package sample;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

public class Main  {

    public static void main(String[] args) {
        /*
         * projectModel is the project class which contains all other classes
         * create instance from project model
         */
        ProjectModel projectModel = new ProjectModel();
        /*
        * Controller -> project controller which will control the creation of the game
        */
        Controller projectController = new Controller(projectModel);

        projectController.createGame();
        projectController.createXoGame();
//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
//        ButtonType rematch = new ButtonType("rematch");
//        alert.getButtonTypes().clear();


    }

}
