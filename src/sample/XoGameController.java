package sample;

import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.FileChooser;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;


public class XoGameController extends Thread {

    private XoGameGui xoGameGui;
    private XoGameBackEnd xoGameBackEnd;
    private Image xImage;
    private Image oImage;
    private List<Button> buttonList;
    private boolean changeTurn = true;
    private boolean gameEnded;
    private FirstBase firstBase;
    private List<String> recordingList;
    private String aiLevelSelector;
    Thread th;
    private XoGameNetworkData networkData;



    public XoGameController(FirstBase first_Base) {
        
        this.firstBase = first_Base;
        System.out.println("XoGameController here");
        this.xoGameBackEnd = new XoGameBackEnd();
        this.buttonList = new ArrayList<>();
        this.recordingList = new ArrayList<>(9);
        this.th = new Thread(this);
        xImage = new Image(getClass().getResource("X.png").toExternalForm());
        oImage = new Image(getClass().getResource("O.png").toExternalForm());
        changeView();


    }

       private void changeView(){
    
        firstBase.playBtn.setOnAction(e -> showview());
        firstBase.networkBtn.setOnAction(e -> showNetview());
    
    }
    
    private void showview(){

        SecondBase view=new SecondBase();
        this.firstBase.WholeGreatPane.setCenter(view);
        view.singlePlayerBtn.setOnAction(e -> viewLevels());
        view.twoPlayersBtn.setOnAction(e -> viewTwoPlayersGame());
        
    }

    /**********************Network Control*********************************/


    private void showNetview(){

        NetworkLoginBase networkLoginBase = new NetworkLoginBase();
        this.firstBase.WholeGreatPane.setCenter(networkLoginBase);
        networkLoginBase.networkLoginBtn.setOnAction(event -> userNetworkData(networkLoginBase.nameText.getText(),networkLoginBase.ipText.getText()));

    }


    private void userNetworkData(String IGN,String serverIp){
        System.out.println(IGN);
       // System.out.println(isValidInet4Address(serverIp));

        if(IGN.equals("")){
            emptyNameAlert();
        }

        if(validate(serverIp)){
            System.out.println("Valid IP");
            xoGameBackEnd.connectOnline(serverIp);
            this.networkData = new XoGameNetworkData();
            th.setDaemon(true);
            th.start();
            viewTwoPlayersGame();
        }else {
            invalidIpAlert();
            System.out.println("Not Valid IP");
        }
       // System.out.println(serverIp);

    }



    /************************ Received packets************************************************/

    @Override
    synchronized public void run() {

        while (true){

            try {

                this.networkData = (XoGameNetworkData) xoGameBackEnd.getObjectInputStream().readObject();
                System.out.println(networkData.getIndex());
                if(networkData.getStartSessionFlag()){
                    continue;
                }
                //System.out.println("networkDataTurn ->" + networkData.isPlayer1Turn());
                if(networkData != null){
                    String finalStr = Integer.toString(networkData.getIndex()) ;
                    ImageView imageView = new ImageView();

                    //   System.out.println("final Str" + finalStr);
                    //  System.out.println("first thread"+finalStr);
                    for (Button guiButton:buttonList){
                        if(guiButton.getId().equals(finalStr) && guiButton.getGraphic() == null ){
                            System.out.println(guiButton.getId());
                            Platform.runLater(new Runnable() {
                                @Override
                                synchronized   public void run() {
                                    //  System.out.println("sent data from server "+finalStr);
                                    if(networkData.isPlayer2Turn() == true){
                                        System.out.println("x image thread");
                                        imageView.setImage(xImage);
                                        guiButton.setGraphic(imageView);
                                        gameEnded=xoGameBackEnd.play(Integer.parseInt(finalStr), changeTurn);
                                        XoGameController.this.changeTurn = false;
                                        return;
                                    }
                                    if(networkData.isPlayer1Turn() == true){
                                        System.out.println("o image thread");
                                        imageView.setImage(oImage);
                                        guiButton.setGraphic(imageView);
                                        gameEnded=xoGameBackEnd.play(Integer.parseInt(finalStr), changeTurn);
                                        XoGameController.this.changeTurn = true;
                                        return;
                                    }

                                    // System.out.println(gameEnded);
                                }
                            });
                        }

                    }
                }else{
                    break;
                }

            } catch (EOFException e){
                Platform.runLater(() -> {
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setTitle("Error Dialog");
                    alert.setHeaderText("Connection Error");
                    alert.setContentText("Server Is Down Try Again Later");
                    ButtonType buttonTypeOne = new ButtonType("Exit");
                    alert.getButtonTypes().setAll(buttonTypeOne);
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == buttonTypeOne){
                        System.exit(0);
                    }
                });
                break;
            } catch (IOException | ClassNotFoundException e) {

                e.printStackTrace();
            }
        }
    }

    /********************** AI Playing **************************/
     private void viewLevels(){
       
        LevelsBase levelsBase=new LevelsBase();
        xoGameBackEnd.setAiIsOn();
        System.out.println(xoGameBackEnd.isAiOn());
        firstBase.WholeGreatPane.setCenter(levelsBase);
        levelsBase.simpleBtn.setOnAction(e -> runEasyLevel());
        levelsBase.meduimBtn.setOnAction(e -> runMediumLevel());
        levelsBase.hardBtn.setOnAction(e -> runHardLevel());

     }




     /***************************Two Players Function ************************/
     
     private void viewTwoPlayersGame(){

      xoGameGui=new XoGameGui();
      addGameButtons();
      firstBase.WholeGreatPane.setCenter(xoGameGui);
     }

    /*********************** AI BLOCK **********************************/
    private void runEasyLevel(){

        aiLevelSelector = "S";
        viewTwoPlayersGame();
    
    }
    
    private void runMediumLevel(){

        aiLevelSelector = "M";
        viewTwoPlayersGame();
    
    }
    
    private void runHardLevel(){

        aiLevelSelector = "H";
        viewTwoPlayersGame();

    }



    private void playVsAi(){

        /*
         *
         * 3shan el random number s3at el l3ba msh bttl3b 3shan 3ml en el o ttl3b ama
         * el button image == null
         * el mfrod el Ai yzbot el kalam da
         * 3shan hwa msh hy5tar makan ml3ob feeeh/
         * kda yb2a el l3b mn el backend l el view etzbt
         * */
        ImageView imageView = new ImageView();
        imageView.setImage(oImage);
        System.out.println("vs Ai Here");
        changeTurn = false;
        System.out.println(aiLevelSelector);
        if(aiLevelSelector.equals("S")){
            String  buttonId = Integer.toString(xoGameBackEnd.easyAi());
            renderAiPlay(buttonId,imageView);
        }
        if(aiLevelSelector.equals("M")){
            String buttonId = Integer.toString(xoGameBackEnd.normalAi());
            renderAiPlay(buttonId,imageView);
        }
        if(aiLevelSelector.equals("H")){
            String buttonId = Integer.toString(xoGameBackEnd.hardAi());
            renderAiPlay(buttonId,imageView);
        }

    }

    /***************** Add Actions To xoBoard *****************/

    public void addGameButtons(){

        buttonList.add(xoGameGui.button0);
        buttonList.add(xoGameGui.button1);
        buttonList.add(xoGameGui.button2);
        buttonList.add(xoGameGui.button3);
        buttonList.add(xoGameGui.button4);
        buttonList.add(xoGameGui.button5);
        buttonList.add(xoGameGui.button6);
        buttonList.add(xoGameGui.button7);
        buttonList.add(xoGameGui.button8);
        viewButtonAction();

    }


    private void viewButtonAction(){
        /*
        * for each button in buttonList call insertTokenImage to add X or O image in that button
        * */
        for (Button b:buttonList){
            b.setOnAction(event -> insertTokenImage(b));
        }


    }


/***********************************Game Control Functions **********************************************/

    private void insertTokenImage(Button button){
        /*
        * insert Token image to the button
        * switch the play of each player
        * send index of token and player turn to XoGameBackEnd class -> check XoGameBackEnd
        *
        * */
        updateScore();
        ImageView imageView = new ImageView();


        if(xoGameBackEnd.isOnline()){
            networkData.setIndex(Integer.parseInt(button.getId()));
            System.out.println("getStartSessionFlag" + networkData.getStartSessionFlag());
            System.out.println("canplay flag " + networkData.isCanPlay());
            if (gameEnded == true){
                winAlertDialog();
                return;
            }
            if(networkData.isCanPlay() && button.getGraphic() ==null){
                try {
                    System.out.println("player1 turn ");

                    System.out.println("canPlay -> flag inside if " + networkData.isCanPlay());
                    xoGameBackEnd.getObjectOutputStream().writeObject(networkData);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                otherPlayerTurnAlert();
            }

        }else {
            if(changeTurn == true && button.getGraphic() ==null && !button.getId().equals("save")){
                imageView.setImage(xImage);
                button.setGraphic(imageView);
                gameEnded =xoGameBackEnd.play( Integer.parseInt(button.getId()) , changeTurn);
                changeTurn = false;
                System.out.println(xoGameBackEnd.isAiOn());
                if(xoGameBackEnd.isAiOn() && !gameEnded){
                    /*
                     * feeeh moshkla fe el win check bt3t el AI 3shan el changeTurn 3la tool b true
                     * fa msh btd5ol 3la el win check bt3t player 2 f el gamebackend
                     *
                     *
                     * */
                    System.out.println("inside ai here take 2");
                    playVsAi();
                    changeTurn = true;
                }
                if(gameEnded == true){
                    winAlertDialog();
                }
//            //System.out.println("button"+button.getId());
                return;
            }
            if(changeTurn == false && button.getGraphic() ==null && !xoGameBackEnd.isAiOn() && !button.getId().equals("save")){
                imageView.setImage(oImage);
                System.out.println("inside btn2 xogamecontroller");
                System.out.println(xoGameBackEnd.isAiOn());
                button.setGraphic(imageView);
                gameEnded = xoGameBackEnd.play( Integer.parseInt(button.getId()) , changeTurn);
                changeTurn = true;
                if(gameEnded == true){
                    winAlertDialog();
                }
//            //System.out.println("button"+button.getId());
                return;
            }
        }


    }

/****************************** Restart Game View And Logic ************************/

    private void clearButtonImage(){
        /*
        * reset backEnd game array with Z token -> dummy token
        * reset button images to null
        * reset player flag to make x start the game again
        *
        * */
        updateScore();
        xoGameBackEnd.restartXoGame();
        recordingList.clear();
        for (Button button : buttonList) {
            button.setGraphic(null);
        }
        this.gameEnded = false;
        this.changeTurn = true;

    }

    private void updateScore(){
        xoGameGui.gamesGuiCounter.setText(Integer.toString(xoGameBackEnd.getGamesCounter()));
        xoGameGui.player1WinsGuiCounter.setText(Integer.toString(xoGameBackEnd.getPlayer1Wins()));
        xoGameGui.player2WinsGuiCounter.setText(Integer.toString(xoGameBackEnd.getPlayer2Wins()));
    }

/************************************** Recording Functions *********************************/
    private void savePlay() {

        String fileSeparator = System.getProperty("file.separator");
        System.out.println(fileSeparator);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-YYYY_HH_mm_ss");
        Date date = new Date();
        System.out.println(dateFormat.format(date));
        String fileName = dateFormat.format(date);
        System.out.println("Records"+fileSeparator+fileName+".rec");
        String relativePath = "Records"+fileSeparator+fileName+".rec";
        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        new File("Records").mkdir();
        File file = new File(relativePath);
        try {
            if(file.createNewFile()){
                System.out.println(relativePath+" File Created in Project Records directory");
                fileWriter = new FileWriter(relativePath,true);
                printWriter = new PrintWriter(fileWriter);
                for (Integer tokenPos: xoGameBackEnd.getRecordingArray()) {
                    printWriter.println(tokenPos);
                }
            }else{
                System.out.println("file bala7a");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                printWriter.close();
                fileWriter.close();
            }catch (IOException e) {
                    e.printStackTrace();
            }

        }

    }



    private void watchGame(){
        
        clearButtonImage();
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);
        File file = new File(selectedFile.toString());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Rec Files", ".rec"));
        try {
            Scanner scanner = new Scanner(file);
            while (scanner.hasNext()){
                recordingList.add(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            synchronized public void run() {
                for (String element: recordingList
                ) {
                    ImageView imageView = new ImageView();
                   // System.out.println(element);

                    if(recordingList.indexOf(element) % 2 == 0){
                       // System.out.println("inside %2");
                        Platform.runLater(new Runnable() {
                            @Override
                            synchronized public void run() {
                                imageView.setImage(xImage);
                                renderWatchPlay(element,imageView);
                            }
                        });
                    }else{
                        Platform.runLater(new Runnable() {
                            @Override
                            synchronized public void run() {
                                imageView.setImage(oImage);
                                renderWatchPlay(element,imageView);
                            }
                        });

                    }

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                 Platform.runLater(new Runnable() {
                    @Override
                    synchronized   public void run() {
                        replayDialog();
                    }
                });
            }

        }).start();
    }

/***************************** Render Functions ****************************/
    private void renderAiPlay(String stringId , ImageView imageView){
        for (Button GuiButton:buttonList){
            if(GuiButton.getId().equals(stringId)){
                System.out.println(stringId);
                GuiButton.setGraphic(imageView);
                gameEnded=xoGameBackEnd.play(Integer.parseInt(stringId),changeTurn);
                break;

            }

        }
    }

   private void renderWatchPlay(String playId, ImageView imageView){
       for (Button GuiButton:buttonList){
           if(GuiButton.getId().equals(playId)){
               GuiButton.setGraphic(imageView);
           }
       }
   }
 /**********************Winning Functions Audio***********************************/


 public void audioFun() {
     URL file=getClass().getResource("Bonus.mp3");
     MediaPlayer player=new MediaPlayer(new Media(file.toString()));
     player.play();
 }


    private void winnerBonus(){

        clearButtonImage();
        BonusClass bonus=new BonusClass();
        if(changeTurn==false){
            bonus.WinnerText.setText("Player One Won");
        }
        if(changeTurn==true){
            bonus.WinnerText.setText("Player Two Won");
        }
        firstBase.WholeGreatPane.setCenter(bonus);
       // clearButtonImage();
        bonus.ExitBtn.setOnAction(e->exit());


    }

    private void exit(){
        //this.firstBase=new FirstBase();
        clearButtonImage();
        firstBase.WholeGreatPane.setCenter(xoGameGui);
        //changeView();

    }

    /************************Alert Function ******************************/


    private void winAlertDialog(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Game Has Been Ended");
        if(xoGameBackEnd.isDrawFlag()){
            alert.setHeaderText("Draw");
        }
        if(changeTurn == false && !xoGameBackEnd.isDrawFlag()){
            alert.setHeaderText("Player 1 Won");
        }
        if(changeTurn == true && !xoGameBackEnd.isDrawFlag()){
            alert.setHeaderText("Player 2 Won");
        }

        ButtonType buttonTypeOne = new ButtonType("Restart");
        ButtonType buttonTypeTwo = new ButtonType("SavePlay");
        ButtonType buttonTypeThree = new ButtonType("WatchGame");
        ButtonType buttonTypefour =  new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeThree,buttonTypefour);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            clearButtonImage();
        }
        if (result.get() == buttonTypeTwo) {
            savePlay();
            saveAlert();
        }
        if (result.get() == buttonTypeThree) {
            watchGame();
        }
        if(result.get() == buttonTypefour){
            System.exit(0);
        }
    }


    private void replayDialog(){
        Alert alert = new Alert(Alert.AlertType.NONE);
        alert.setTitle("RePlay has Been Ended");

        alert.setContentText("Choose your option.");
        ButtonType buttonTypeOne = new ButtonType("Play");
        ButtonType buttonTypeThree = new ButtonType("WatchGame");
        ButtonType buttonTypefour =  new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeThree,buttonTypefour);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            clearButtonImage();
        }

        if (result.get() == buttonTypeThree) {
            watchGame();
        }
        if(result.get() == buttonTypefour){
            System.exit(0);
        }
    }


    private void saveAlert(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Save Dialog");
        alert.setHeaderText("Save completed");

        alert.setContentText("Choose your option.");
        ButtonType buttonTypeOne = new ButtonType("Restart");
        ButtonType buttonTypeThree = new ButtonType("WatchGame");
        ButtonType buttonTypefour =  new ButtonType("Exit");

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeThree,buttonTypefour);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            clearButtonImage();
        }

        if (result.get() == buttonTypeThree) {
            watchGame();
        }
        if(result.get() == buttonTypefour){
            System.exit(0);
        }
    }

    private void otherPlayerTurnAlert(){
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Wrong Move");
        alert.setHeaderText("Other Player Move");

        alert.showAndWait();
    }

    private void emptyNameAlert(){

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Invalid Data");
        alert.setHeaderText("Please Enter an IGN");
        alert.showAndWait();

    }


    private void invalidIpAlert (){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error Dialog");
        alert.setHeaderText("InValid ip ");
        alert.setContentText("Ooops, Enter a Valid Ip");

        alert.showAndWait();
    }

    /***********************Network Ip Validator****************************/
    private static final Pattern PATTERN = Pattern.compile(
            "^(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.){3}([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");

    public static boolean validate(final String ip) {
        return PATTERN.matcher(ip).matches();
    }


}
