package sample;

import static java.lang.Integer.max;
import static java.lang.Integer.min;
import java.util.List;

public class AI {

    private final HumanPlayer.Type gameToken;
    private boolean turn;
    private boolean aiOn;
    // private final HumanPlayer.Type mySeed;

    public AI(HumanPlayer.Type gameToken) {
        this.gameToken = gameToken;
        this.aiOn = false;
    }

    public int getEasyAi(HumanPlayer.Type[] gameArray) {

        System.out.println("Easy Mode Here");
        int prefered_moves[] = {4, 0, 2, 6, 8, 1, 3, 5, 7};
        int move;
        for (move = 0; gameArray[prefered_moves[move]] != HumanPlayer.Type.Z && move < 9; move++) {
            if (gameArray[prefered_moves[move]] == HumanPlayer.Type.X) {
                continue;
            }
        }
        return prefered_moves[move];
        
    }

    public int getNormalAi(HumanPlayer.Type[] gameArray) {
        System.out.println("Normal Mode Here");
        
         int x = (int) (Math.random()* 8 + 0);
        while (gameArray[x] != HumanPlayer.Type.Z ){
            x = (int) (Math.random()* 8 + 0);
            if(gameArray[x] == HumanPlayer.Type.X){
                continue;
            }
        }

        return x;
    }

    public int getHardAi(HumanPlayer.Type[] gameArray) {
        System.out.println("Hard Mode Here");
         class Ai_InnerClass {

            HumanPlayer.Type player = HumanPlayer.Type.O;
            HumanPlayer.Type opponent = HumanPlayer.Type.X;

            // This function returns true if there are moves
            // remaining on the board. It returns false if
            // there are no moves left to play.
            public boolean isMovesLeft(HumanPlayer.Type[] board) {
                for (int i = 0; i < 9; i++) {
                    if (board[i] == HumanPlayer.Type.Z) {

                        return true;
                    }
                }
                return false;
            }

            // This is the evaluation function as discussed

            public int evaluate(HumanPlayer.Type[] board) {
                // Checking for Rows for X or O victory.

                if ((board[0] == board[1] && board[0] == board[2])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[3] == board[4] && board[3] == board[5])) {
                    if (board[3] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[3] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[6] == board[7] && board[6] == board[8])) {
                    if (board[6] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[6] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Checking for Columns for X or O victory.
                if ((board[0] == board[3] && board[0] == board[6])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[1] == board[4] && board[1] == board[7])) {
                    if (board[1] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[1] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[2] == board[5] && board[2] == board[8])) {
                    if (board[2] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[2] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Checking for Diagonals for X or O victory.
                if ((board[0] == board[4] && board[0] == board[8])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[2] == board[4] && board[2] == board[6])) {
                    if (board[2] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[2] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Else if none of them have won then return 0
                return 0;
            }

            // This is the minimax function. It considers all
            // the possible ways the game can go and returns
            // the value of the board
            int minimax(HumanPlayer.Type[] board, int depth, boolean isMax) {
                int score = evaluate(board);

                // If Maximizer has won the game return his/her
                // evaluated score
                if (score == 10) {
                    return score;
                }

                // If Minimizer has won the game return his/her
                // evaluated score
                if (score == -10) {
                    return score;
                }

                // If there are no more moves and no winner then
                // it is a tie
                if (isMovesLeft(board) == false) {
                    return 0;
                }

                // If this maximizer's move
                if (isMax) {
                    int best = -1000;

                    // Traverse all cells
                    for (int i = 0; i < 9; i++) {
                        // Check if cell is empty
                        if (board[i] == HumanPlayer.Type.Z) {
                            // Make the move
                            board[i] = player;

                            // Call minimax recursively and choose
                            // the maximum value
                            best = max(best, minimax(board, depth + 1, !isMax));

                            // Undo the move
                            board[i] = HumanPlayer.Type.Z;
                        }

                    }
                    return best;
                } // If this minimizer's move
                else {
                    int best = 1000;

                    // Traverse all cells
                    for (int i = 0; i < 9; i++) {
                        // Check if cell is empty
                        if (board[i] == HumanPlayer.Type.Z) {
                            // Make the move
                            board[i] = opponent;

                            // Call minimax recursively and choose
                            // the minimum value
                            best = min(best, minimax(board, depth + 1, !isMax));

                            // Undo the move
                            board[i] = HumanPlayer.Type.Z;
                        }

                    }
                    return best;
                }
            }

            public int findBestMove(HumanPlayer.Type[] board)
            {
                int bestVal = -1000;
                int bestMove;
                bestMove = -1;

                // Traverse all cells, evaluate minimax function for
                // all empty cells. And return the cell with optimal
                // value.
                for (int i = 0; i < 9; i++) {
                    // Check if cell is empty
                    if (board[i] ==HumanPlayer.Type.Z) {
                        // Make the move
                        board[i]= player;

                        // compute evaluation function for this
                        // move.
                        int moveVal = minimax(board, 0, false);

                        // Undo the move
                        board[i] = HumanPlayer.Type.Z;

                        // If the value of the current move is
                        // more than the best value, then update
                        // best/
                        if (moveVal > bestVal) {
                            bestMove = i;
                            bestVal = moveVal;
                        }
                    }

                }


                System.out.println("The value of the best Move is :"+bestVal);

                return bestMove;
            }

        }


        Ai_InnerClass obj=new Ai_InnerClass();

        int best_move=obj.findBestMove(gameArray);

        return best_move;
    }

    /**
     * **** Medium Level **
     */
//    public int aiPlay(HumanPlayer.Type[] gameArray){
//        // HumanPlayer.Type[] gameArray should be the argument but will be after that test
//        int x = (int) (Math.random()* 8 + 0);
//        while (gameArray[x] != HumanPlayer.Type.Z ){
//            x = (int) (Math.random()* 8 + 0);
//            if(gameArray[x] == HumanPlayer.Type.X){
//                continue;
//            }
//        }
//
//        return x;
//    }
    /**
     * ** Easy level ******
     */
//    public int aiPlay(HumanPlayer.Type[] gameArray) {
//        int prefered_moves[] = {4, 0, 2, 6, 8, 1, 3, 5, 7};
//        int move;
//        for (move = 0; gameArray[prefered_moves[move]] != HumanPlayer.Type.Z && move < 9; move++) {
//            if (gameArray[prefered_moves[move]] == HumanPlayer.Type.X) {
//                continue;
//            }
//        }
//        return prefered_moves[move];
//    }
    public int aiPlay(HumanPlayer.Type[] gameArray) {

        class Ai_InnerClass {

            HumanPlayer.Type player = HumanPlayer.Type.O;
            HumanPlayer.Type opponent = HumanPlayer.Type.X;

            // This function returns true if there are moves
            // remaining on the board. It returns false if
            // there are no moves left to play.
            public boolean isMovesLeft(HumanPlayer.Type[] board) {
                for (int i = 0; i < 9; i++) {
                    if (board[i] == HumanPlayer.Type.Z) {

                        return true;
                    }
                }
                return false;
            }

            // This is the evaluation function as discussed

            public int evaluate(HumanPlayer.Type[] board) {
                // Checking for Rows for X or O victory.

                if ((board[0] == board[1] && board[0] == board[2])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[3] == board[4] && board[3] == board[5])) {
                    if (board[3] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[3] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[6] == board[7] && board[6] == board[8])) {
                    if (board[6] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[6] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Checking for Columns for X or O victory.
                if ((board[0] == board[3] && board[0] == board[6])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[1] == board[4] && board[1] == board[7])) {
                    if (board[1] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[1] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[2] == board[5] && board[2] == board[8])) {
                    if (board[2] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[2] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Checking for Diagonals for X or O victory.
                if ((board[0] == board[4] && board[0] == board[8])) {
                    if (board[0] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[0] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                if ((board[2] == board[4] && board[2] == board[6])) {
                    if (board[2] == HumanPlayer.Type.O) {
                        return +10;
                    } else if (board[2] == HumanPlayer.Type.X) {
                        return -10;
                    }
                }

                // Else if none of them have won then return 0
                return 0;
            }

            // This is the minimax function. It considers all
            // the possible ways the game can go and returns
            // the value of the board
            int minimax(HumanPlayer.Type[] board, int depth, boolean isMax) {
                int score = evaluate(board);

                // If Maximizer has won the game return his/her
                // evaluated score
                if (score == 10) {
                    return score;
                }

                // If Minimizer has won the game return his/her
                // evaluated score
                if (score == -10) {
                    return score;
                }

                // If there are no more moves and no winner then
                // it is a tie
                if (isMovesLeft(board) == false) {
                    return 0;
                }

                // If this maximizer's move
                if (isMax) {
                    int best = -1000;

                    // Traverse all cells
                    for (int i = 0; i < 9; i++) {
                        // Check if cell is empty
                        if (board[i] == HumanPlayer.Type.Z) {
                            // Make the move
                            board[i] = player;

                            // Call minimax recursively and choose
                            // the maximum value
                            best = max(best, minimax(board, depth + 1, !isMax));

                            // Undo the move
                            board[i] = HumanPlayer.Type.Z;
                        }

                    }
                    return best;
                } // If this minimizer's move
                else {
                    int best = 1000;

                    // Traverse all cells
                    for (int i = 0; i < 9; i++) {
                        // Check if cell is empty
                        if (board[i] == HumanPlayer.Type.Z) {
                            // Make the move
                            board[i] = opponent;

                            // Call minimax recursively and choose
                            // the minimum value
                            best = min(best, minimax(board, depth + 1, !isMax));

                            // Undo the move
                            board[i] = HumanPlayer.Type.Z;
                        }

                    }
                    return best;
                }
            }

            public int findBestMove(HumanPlayer.Type[] board)
            {
                int bestVal = -1000;
                int bestMove;
                bestMove = -1;

                // Traverse all cells, evaluate minimax function for
                // all empty cells. And return the cell with optimal
                // value.
                for (int i = 0; i < 9; i++) {
                    // Check if cell is empty
                    if (board[i] ==HumanPlayer.Type.Z) {
                        // Make the move
                        board[i]= player;

                        // compute evaluation function for this
                        // move.
                        int moveVal = minimax(board, 0, false);

                        // Undo the move
                        board[i] = HumanPlayer.Type.Z;

                        // If the value of the current move is
                        // more than the best value, then update
                        // best/
                        if (moveVal > bestVal) {
                            bestMove = i;
                            bestVal = moveVal;
                        }
                    }

                }


                System.out.println("The value of the best Move is :"+bestVal);

                return bestMove;
            }

        }


        Ai_InnerClass obj=new Ai_InnerClass();

        int best_move=obj.findBestMove(gameArray);

        return best_move;

    }

    public HumanPlayer.Type getGameToken() {
        return gameToken;
    }

    public boolean isAiOn() {
        return aiOn;
    }

    public void setAiOn(boolean aiOn) {
        this.aiOn = aiOn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    public boolean isTurn() {
        return turn;
    }

}



