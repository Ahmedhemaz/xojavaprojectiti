package sample;

public class Game {

    private XoGameBackEnd xoGameBackEnd;
    private XoGameGuiEx xoGameGuiEx;

    public Game() {
        this.xoGameGuiEx = new XoGameGuiEx();
    }

    public void createXoGameBackEndVsHumanPlayer(HumanPlayer player1,HumanPlayer player2){
        this.xoGameBackEnd = new XoGameBackEnd(player1,player2);

    }

    public void createXoGameBackEndVsAiPlayer(HumanPlayer player1,AI aiPlayer){
        this.xoGameBackEnd = new XoGameBackEnd(player1,aiPlayer);

    }

    public void launchXoGameGui()
    {
        this.xoGameGuiEx.main(null);

    }

    public XoGameGuiEx getXoGameGuiEx() {

        return xoGameGuiEx;
    }

    public XoGameBackEnd getXoGameBackEnd() {
        return xoGameBackEnd;
    }
}
