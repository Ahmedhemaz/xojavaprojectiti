package sample;

public class Controller {

    ProjectModel projectModel;

    public Controller(ProjectModel projectModel) {

        /*
        * Controller constructor will point this.projectModel reference to the created projectModel in main function
        *
        * */
        this.projectModel = projectModel;
    }

    public void createGame(){
        /*
         * create new game instance which will contain all games
         */
        projectModel.createGame();
    }

    public void createXoGame(){

        /*
        * will launch the gui main method -> check XoGameGuiEx
        *
        * */
            projectModel.getGame().launchXoGameGui();
    }







}
