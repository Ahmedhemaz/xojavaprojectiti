package sample;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class RequestBase extends Pane {

    protected final Pane pane;
    protected final Text text;
    protected final Text text0;
    protected final Text text1;
    protected final Text playerName;
    protected final Text text2;
    protected final Button rejectBtn;
    protected final Reflection reflection;
    protected final Text text3;
    protected final Button acceptBtn;
    protected final Reflection reflection0;

    public RequestBase() {

        pane = new Pane();
        text = new Text();
        text0 = new Text();
        text1 = new Text();
        playerName = new Text();
        text2 = new Text();
        rejectBtn = new Button();
        reflection = new Reflection();
        text3 = new Text();
        acceptBtn = new Button();
        reflection0 = new Reflection();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(553.0);
        setPrefWidth(469.0);
        setStyle("-fx-background-color: #080d21;");

        pane.setLayoutX(51.0);
        pane.setLayoutY(43.0);
        pane.setPrefHeight(473.0);
        pane.setPrefWidth(371.0);
        pane.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(89.0);
        text.setLayoutY(88.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold", 34.0));

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(152.0);
        text0.setLayoutY(86.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(63.13671875);
        text0.setFont(new Font("System Bold", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(227.0);
        text1.setLayoutY(85.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(73.13671875);
        text1.setFont(new Font("System Bold", 34.0));

        playerName.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        playerName.setLayoutX(47.0);
        playerName.setLayoutY(178.0);
        playerName.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        playerName.setStrokeWidth(0.0);
        playerName.setText("Name");
        playerName.setWrappingWidth(73.13671784102917);
        playerName.setFont(new Font("System Bold", 24.0));

        text2.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text2.setLayoutX(120.0);
        text2.setLayoutY(180.0);
        text2.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text2.setStrokeWidth(0.0);
        text2.setText("Sended A Request ");
        text2.setWrappingWidth(242.13671875);
        text2.setFont(new Font("System Bold", 24.0));

        rejectBtn.setLayoutX(236.0);
        rejectBtn.setLayoutY(334.0);
        rejectBtn.setMnemonicParsing(false);
        rejectBtn.setPrefHeight(45.0);
        rejectBtn.setPrefWidth(82.0);
        rejectBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        rejectBtn.getStyleClass().add("btn");
        rejectBtn.setText("No");
        rejectBtn.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        rejectBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 24.0));
        rejectBtn.setCursor(Cursor.HAND);

        rejectBtn.setEffect(reflection);

        text3.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text3.setLayoutX(43.0);
        text3.setLayoutY(252.0);
        text3.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text3.setStrokeWidth(0.0);
        text3.setText("Do You Accept  To Play With ?");
        text3.setWrappingWidth(291.13671875);
        text3.setFont(new Font("System Bold", 18.0));

        acceptBtn.setLayoutX(48.0);
        acceptBtn.setLayoutY(334.0);
        acceptBtn.setMnemonicParsing(false);
        acceptBtn.setPrefHeight(45.0);
        acceptBtn.setPrefWidth(82.0);
        acceptBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        acceptBtn.getStyleClass().add("btn");
        acceptBtn.setText("Yes");
        acceptBtn.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        acceptBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 24.0));
        acceptBtn.setCursor(Cursor.HAND);

        acceptBtn.setEffect(reflection0);

        pane.getChildren().add(text);
        pane.getChildren().add(text0);
        pane.getChildren().add(text1);
        pane.getChildren().add(playerName);
        pane.getChildren().add(text2);
        pane.getChildren().add(rejectBtn);
        pane.getChildren().add(text3);
        pane.getChildren().add(acceptBtn);
        getChildren().add(pane);

    }
}
