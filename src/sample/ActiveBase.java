package sample;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class ActiveBase extends Pane {

    protected final Pane pane;
    protected final Text text;
    protected final Button player1Name;
    protected final Reflection reflection;
    protected final Text text0;
    protected final Text text1;
    protected final Text text2;
    protected final Button player2Name;
    protected final Reflection reflection0;

    public ActiveBase() {

        pane = new Pane();
        text = new Text();
        player1Name = new Button();
        reflection = new Reflection();
        text0 = new Text();
        text1 = new Text();
        text2 = new Text();
        player2Name = new Button();
        reflection0 = new Reflection();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(553.0);
        setPrefWidth(469.0);
        setStyle("-fx-background-color: #080d21;");

        pane.setLayoutX(51.0);
        pane.setLayoutY(43.0);
        pane.setPrefHeight(473.0);
        pane.setPrefWidth(371.0);
        pane.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(89.0);
        text.setLayoutY(88.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold", 34.0));

        player1Name.setLayoutX(52.0);
        player1Name.setLayoutY(181.0);
        player1Name.setMaxHeight(20.0);
        player1Name.setMaxWidth(200.0);
        player1Name.setMinWidth(20.0);
        player1Name.setMnemonicParsing(false);
        player1Name.setPrefHeight(20.0);
        player1Name.setPrefWidth(183.0);
        player1Name.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px; -fx-max-height: 50px;");
        player1Name.getStyleClass().add("btn");
        player1Name.setText(" Player1");
        player1Name.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        player1Name.setFont(new Font("Tw Cen MT Condensed Extra Bold", 18.0));
        player1Name.setCursor(Cursor.HAND);

        player1Name.setEffect(reflection);

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(152.0);
        text0.setLayoutY(86.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(63.13671875);
        text0.setFont(new Font("System Bold", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(227.0);
        text1.setLayoutY(85.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(73.13671875);
        text1.setFont(new Font("System Bold", 34.0));

        text2.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text2.setLayoutX(51.0);
        text2.setLayoutY(155.0);
        text2.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text2.setStrokeWidth(0.0);
        text2.setText("Active Now");
        text2.setWrappingWidth(235.13671875);
        text2.setFont(new Font("System Bold", 24.0));

        player2Name.setLayoutX(50.0);
        player2Name.setLayoutY(245.0);
        player2Name.setMaxHeight(20.0);
        player2Name.setMaxWidth(200.0);
        player2Name.setMinWidth(20.0);
        player2Name.setMnemonicParsing(false);
        player2Name.setPrefHeight(20.0);
        player2Name.setPrefWidth(183.0);
        player2Name.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px; -fx-max-height: 50px;");
        player2Name.getStyleClass().add("btn");
        player2Name.setText(" Player2");
        player2Name.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        player2Name.setFont(new Font("Tw Cen MT Condensed Extra Bold", 18.0));
        player2Name.setCursor(Cursor.HAND);

        player2Name.setEffect(reflection0);

        pane.getChildren().add(text);
        pane.getChildren().add(player1Name);
        pane.getChildren().add(text0);
        pane.getChildren().add(text1);
        pane.getChildren().add(text2);
        pane.getChildren().add(player2Name);
        getChildren().add(pane);

    }
}
