package sample;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class SecondBase extends Pane {

    protected final Pane pane;
    protected final Text text;
    protected final Button singlePlayerBtn;
    protected final Reflection reflection;
    protected final Button twoPlayersBtn;
    protected final Reflection reflection0;
    protected final Text text0;
    protected final Text text1;

    public SecondBase() {

        pane = new Pane();
        text = new Text();
        singlePlayerBtn = new Button();
        reflection = new Reflection();
        twoPlayersBtn = new Button();
        reflection0 = new Reflection();
        text0 = new Text();
        text1 = new Text();

        setMaxHeight(USE_PREF_SIZE);
        setMaxWidth(USE_PREF_SIZE);
        setMinHeight(USE_PREF_SIZE);
        setMinWidth(USE_PREF_SIZE);
        setPrefHeight(553.0);
        setPrefWidth(469.0);
        setStyle("-fx-background-color: #080d21;");

        pane.setLayoutX(51.0);
        pane.setLayoutY(43.0);
        pane.setPrefHeight(473.0);
        pane.setPrefWidth(371.0);
        pane.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(89.0);
        text.setLayoutY(88.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold", 34.0));

        singlePlayerBtn.setLayoutX(52.0);
        singlePlayerBtn.setLayoutY(182.0);
        singlePlayerBtn.setMnemonicParsing(false);
        singlePlayerBtn.setPrefHeight(69.0);
        singlePlayerBtn.setPrefWidth(264.0);
        singlePlayerBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        singlePlayerBtn.getStyleClass().add("btn");
        singlePlayerBtn.setText("Single Player");
        singlePlayerBtn.setTextFill(javafx.scene.paint.Color.valueOf("#a7e2f8"));
        singlePlayerBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        singlePlayerBtn.setCursor(Cursor.HAND);

        singlePlayerBtn.setEffect(reflection);

        twoPlayersBtn.setLayoutX(51.0);
        twoPlayersBtn.setLayoutY(311.0);
        twoPlayersBtn.setMnemonicParsing(false);
        twoPlayersBtn.setPrefHeight(69.0);
        twoPlayersBtn.setPrefWidth(264.0);
        twoPlayersBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #cc59bb; -fx-border-style: solid; -fx-border-width: 4px;");
        twoPlayersBtn.getStyleClass().add("btn");
        twoPlayersBtn.setText("Two Players");
        twoPlayersBtn.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        twoPlayersBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        twoPlayersBtn.setCursor(Cursor.HAND);

        twoPlayersBtn.setEffect(reflection0);

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(152.0);
        text0.setLayoutY(86.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(63.13671875);
        text0.setFont(new Font("System Bold", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(227.0);
        text1.setLayoutY(85.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(73.13671875);
        text1.setFont(new Font("System Bold", 34.0));

        pane.getChildren().add(text);
        pane.getChildren().add(singlePlayerBtn);
        pane.getChildren().add(twoPlayersBtn);
        pane.getChildren().add(text0);
        pane.getChildren().add(text1);
        getChildren().add(pane);
        
        
//        singlePlayerBtn.setOnAction(new EventHandler<ActionEvent>() {
//        public void handle(ActionEvent event) {
//         //System.out.println("About");
//         LevelsBase Parent=new LevelsBase();
//         Scene scecondscene = new Scene(Parent);
//         Stage widow=(Stage)((Node)event.getSource()).getScene().getWindow();
//        
//         widow.setScene(scecondscene);
//         widow.show();
//        }
//        });
//        
        
//        twoPlayersBtn.setOnAction(new EventHandler<ActionEvent>() {
//        public void handle(ActionEvent event) {
//       
//         XoGameGui forthsceneParent =new XoGameGui();
//         
//         XoGameController con=new XoGameController(new XoGameGui());
//         forthsceneParent=con.getXoGameGui();
//         
//         Scene forthscene = new Scene(forthsceneParent);
//         Stage widow=(Stage)((Node)event.getSource()).getScene().getWindow();
//        
//         widow.setScene(forthscene);
//         widow.show();
//
//
////          ProjectModel projectModel = new ProjectModel();
////        /*
////        * Controller -> project controller which will control the creation of the game
////        */
////          Controller projectController = new Controller(projectModel);
////
////          projectController.createGame();
////          projectController.createXoGame();
//        }
//        });;

    }
}
