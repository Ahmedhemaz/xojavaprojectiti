package sample;

import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public abstract class AbstractXo2GameGui extends BorderPane {

    protected final GridPane gridPane;
    protected final ColumnConstraints columnConstraints;
    protected final ColumnConstraints columnConstraints0;
    protected final ColumnConstraints columnConstraints1;
    protected final RowConstraints rowConstraints;
    protected final RowConstraints rowConstraints0;
    protected final RowConstraints rowConstraints1;
    protected final Button button0;
    protected final ImageView imageView;
    protected final Button button1;
    protected final ImageView image1;
    protected final Button button2;
    protected final ImageView image2;
    protected final Button button3;
    protected final ImageView image3;
    protected final Button button4;
    protected final ImageView image4;
    protected final Button button5;
    protected final ImageView image5;
    protected final Button button6;
    protected final ImageView image6;
    protected final Button button7;
    protected final ImageView image7;
    protected final Button button8;
    protected final ImageView image8;
    protected final Button button;
    protected final GridPane GameInfoGridPane;
    protected final ColumnConstraints columnConstraints2;
    protected final ColumnConstraints columnConstraints3;
    protected final ColumnConstraints columnConstraints4;
    protected final RowConstraints rowConstraints2;
    protected final GridPane GamesCounterGridPane;
    protected final ColumnConstraints columnConstraints5;
    protected final ColumnConstraints columnConstraints6;
    protected final RowConstraints rowConstraints3;
    protected final Text gamesCounterField;
    protected final Text gamesGuiCounter;
    protected final GridPane Player1WinsGridPan;
    protected final ColumnConstraints columnConstraints7;
    protected final ColumnConstraints columnConstraints8;
    protected final RowConstraints rowConstraints4;
    protected final Text player1Field;
    protected final Text player1WinsGuiCounter;
    protected final GridPane Player2WinsGridPan;
    protected final ColumnConstraints columnConstraints9;
    protected final ColumnConstraints columnConstraints10;
    protected final RowConstraints rowConstraints5;
    protected final Text player2Field;
    protected final Text player2WinsGuiCounter0;

    public AbstractXo2GameGui() {

        gridPane = new GridPane();
        columnConstraints = new ColumnConstraints();
        columnConstraints0 = new ColumnConstraints();
        columnConstraints1 = new ColumnConstraints();
        rowConstraints = new RowConstraints();
        rowConstraints0 = new RowConstraints();
        rowConstraints1 = new RowConstraints();
        button0 = new Button();
        imageView = new ImageView();
        button1 = new Button();
        image1 = new ImageView();
        button2 = new Button();
        image2 = new ImageView();
        button3 = new Button();
        image3 = new ImageView();
        button4 = new Button();
        image4 = new ImageView();
        button5 = new Button();
        image5 = new ImageView();
        button6 = new Button();
        image6 = new ImageView();
        button7 = new Button();
        image7 = new ImageView();
        button8 = new Button();
        image8 = new ImageView();
        button = new Button();
        GameInfoGridPane = new GridPane();
        columnConstraints2 = new ColumnConstraints();
        columnConstraints3 = new ColumnConstraints();
        columnConstraints4 = new ColumnConstraints();
        rowConstraints2 = new RowConstraints();
        GamesCounterGridPane = new GridPane();
        columnConstraints5 = new ColumnConstraints();
        columnConstraints6 = new ColumnConstraints();
        rowConstraints3 = new RowConstraints();
        gamesCounterField = new Text();
        gamesGuiCounter = new Text();
        Player1WinsGridPan = new GridPane();
        columnConstraints7 = new ColumnConstraints();
        columnConstraints8 = new ColumnConstraints();
        rowConstraints4 = new RowConstraints();
        player1Field = new Text();
        player1WinsGuiCounter = new Text();
        Player2WinsGridPan = new GridPane();
        columnConstraints9 = new ColumnConstraints();
        columnConstraints10 = new ColumnConstraints();
        rowConstraints5 = new RowConstraints();
        player2Field = new Text();
        player2WinsGuiCounter0 = new Text();

        gridPane.setMaxHeight(USE_PREF_SIZE);
        gridPane.setMaxWidth(USE_PREF_SIZE);
        gridPane.setMinHeight(USE_PREF_SIZE);
        gridPane.setMinWidth(USE_PREF_SIZE);
        gridPane.setPrefHeight(480.0);
        gridPane.setPrefWidth(640.0);

        columnConstraints.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints.setMinWidth(10.0);
        columnConstraints.setPrefWidth(100.0);

        columnConstraints0.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints0.setMinWidth(10.0);
        columnConstraints0.setPrefWidth(100.0);

        columnConstraints1.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints1.setMinWidth(10.0);
        columnConstraints1.setPrefWidth(100.0);

        rowConstraints.setMinHeight(10.0);
        rowConstraints.setPrefHeight(30.0);
        rowConstraints.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints0.setMinHeight(10.0);
        rowConstraints0.setPrefHeight(30.0);
        rowConstraints0.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        rowConstraints1.setMinHeight(10.0);
        rowConstraints1.setPrefHeight(30.0);
        rowConstraints1.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        button0.setMnemonicParsing(false);
        button0.setPrefHeight(168.0);
        button0.setPrefWidth(212.0);
        button0.setStyle("-fx-background-image: 182;");

        imageView.setFitHeight(150.0);
        imageView.setFitWidth(150.0);
        imageView.setPickOnBounds(true);
        imageView.setPreserveRatio(true);
        imageView.setStyle("-fx-opacity: 1;");
        //imageView.setImage(new Image(getClass().getResource("X.png").toExternalForm()));
        //button0.setGraphic(imageView);
        button0.setId("0");


        GridPane.setColumnIndex(button1, 1);
        button1.setMnemonicParsing(false);
        button1.setPrefHeight(191.0);
        button1.setPrefWidth(271.0);
        button1.setId("1");

        image1.setFitHeight(150.0);
        image1.setFitWidth(150.0);
        image1.setPickOnBounds(true);
        image1.setPreserveRatio(true);
       // image1.setImage(new Image(getClass().getResource("../../../../../JavaProjectTest3/src/sample/O.png").toExternalForm()));
        //button1.setGraphic(image1);


        GridPane.setColumnIndex(button2, 2);
        button2.setMnemonicParsing(false);
        button2.setPrefHeight(201.0);
        button2.setPrefWidth(250.0);
        button2.setId("2");
        image2.setFitHeight(110.0);
        image2.setFitWidth(182.0);
        image2.setPickOnBounds(true);
        image2.setPreserveRatio(true);
       // button2.setGraphic(image2);

        GridPane.setRowIndex(button3, 1);
        button3.setMnemonicParsing(false);
        button3.setPrefHeight(231.0);
        button3.setPrefWidth(298.0);
        button3.setId("3");

        image3.setFitHeight(110.0);
        image3.setFitWidth(182.0);
        image3.setPickOnBounds(true);
        image3.setPreserveRatio(true);
        //button3.setGraphic(image3);

        GridPane.setColumnIndex(button4, 1);
        GridPane.setRowIndex(button4, 1);
        button4.setMnemonicParsing(false);
        button4.setPrefHeight(185.0);
        button4.setPrefWidth(289.0);
        button4.setId("4");

        image4.setFitHeight(110.0);
        image4.setFitWidth(182.0);
        image4.setPickOnBounds(true);
        image4.setPreserveRatio(true);
        //button4.setGraphic(image4);

        GridPane.setColumnIndex(button5, 2);
        GridPane.setRowIndex(button5, 1);
        button5.setMnemonicParsing(false);
        button5.setPrefHeight(191.0);
        button5.setPrefWidth(337.0);
        button5.setId("5");

        image5.setFitHeight(110.0);
        image5.setFitWidth(182.0);
        image5.setPickOnBounds(true);
        image5.setPreserveRatio(true);
        //button5.setGraphic(image5);

        GridPane.setRowIndex(button6, 2);
        button6.setMnemonicParsing(false);
        button6.setPrefHeight(262.0);
        button6.setPrefWidth(329.0);
        button6.setId("6");

        image6.setFitHeight(110.0);
        image6.setFitWidth(182.0);
        image6.setPickOnBounds(true);
        image6.setPreserveRatio(true);
       // button6.setGraphic(image6);

        GridPane.setColumnIndex(button7, 1);
        GridPane.setRowIndex(button7, 2);
        button7.setMnemonicParsing(false);
        button7.setPrefHeight(257.0);
        button7.setPrefWidth(316.0);
        button7.setId("7");

        image7.setFitHeight(110.0);
        image7.setFitWidth(182.0);
        image7.setPickOnBounds(true);
        image7.setPreserveRatio(true);
        //button7.setGraphic(image7);

        GridPane.setColumnIndex(button8, 2);
        GridPane.setRowIndex(button8, 2);
        button8.setMnemonicParsing(false);
        button8.setPrefHeight(331.0);
        button8.setPrefWidth(386.0);
        button8.setId("8");

        image8.setFitHeight(110.0);
        image8.setFitWidth(182.0);
        image8.setPickOnBounds(true);
        image8.setPreserveRatio(true);
       // button8.setGraphic(image8);
        setCenter(gridPane);

        BorderPane.setAlignment(button, javafx.geometry.Pos.CENTER);
        button.setMnemonicParsing(false);
        button.setPrefHeight(59.0);
        button.setPrefWidth(639.0);
        button.setText("Restart");
        setBottom(button);

        BorderPane.setAlignment(GameInfoGridPane, javafx.geometry.Pos.CENTER);

        columnConstraints2.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints2.setMinWidth(10.0);
        columnConstraints2.setPrefWidth(100.0);

        columnConstraints3.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints3.setMinWidth(10.0);
        columnConstraints3.setPrefWidth(100.0);

        columnConstraints4.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints4.setMinWidth(10.0);
        columnConstraints4.setPrefWidth(100.0);

        rowConstraints2.setMinHeight(10.0);
        rowConstraints2.setPrefHeight(30.0);
        rowConstraints2.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        columnConstraints5.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints5.setMinWidth(10.0);
        columnConstraints5.setPrefWidth(100.0);

        columnConstraints6.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints6.setMinWidth(10.0);
        columnConstraints6.setPrefWidth(100.0);

        rowConstraints3.setMinHeight(10.0);
        rowConstraints3.setPrefHeight(30.0);
        rowConstraints3.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        gamesCounterField.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        gamesCounterField.setStrokeWidth(0.0);
        gamesCounterField.setText("Games");
        gamesCounterField.setFont(new Font(18.0));

        GridPane.setColumnIndex(gamesGuiCounter, 1);
        gamesGuiCounter.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        gamesGuiCounter.setStrokeWidth(0.0);
        gamesGuiCounter.setText("No.");
        gamesGuiCounter.setFont(new Font(18.0));

        GridPane.setColumnIndex(Player1WinsGridPan, 1);

        columnConstraints7.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints7.setMinWidth(10.0);
        columnConstraints7.setPrefWidth(100.0);

        columnConstraints8.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints8.setMinWidth(10.0);
        columnConstraints8.setPrefWidth(100.0);

        rowConstraints4.setMinHeight(10.0);
        rowConstraints4.setPrefHeight(30.0);
        rowConstraints4.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        player1Field.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        player1Field.setStrokeWidth(0.0);
        player1Field.setText("Player1:");
        player1Field.setFont(new Font(18.0));

        GridPane.setColumnIndex(player1WinsGuiCounter, 1);
        player1WinsGuiCounter.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        player1WinsGuiCounter.setStrokeWidth(0.0);
        player1WinsGuiCounter.setText("Player1Wins");
        player1WinsGuiCounter.setFont(new Font(14.0));

        GridPane.setColumnIndex(Player2WinsGridPan, 2);

        columnConstraints9.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints9.setMinWidth(10.0);
        columnConstraints9.setPrefWidth(100.0);

        columnConstraints10.setHgrow(javafx.scene.layout.Priority.SOMETIMES);
        columnConstraints10.setMinWidth(10.0);
        columnConstraints10.setPrefWidth(100.0);

        rowConstraints5.setMinHeight(10.0);
        rowConstraints5.setPrefHeight(30.0);
        rowConstraints5.setVgrow(javafx.scene.layout.Priority.SOMETIMES);

        player2Field.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        player2Field.setStrokeWidth(0.0);
        player2Field.setText("Player2");
        player2Field.setFont(new Font(18.0));

        GridPane.setColumnIndex(player2WinsGuiCounter0, 1);
        player2WinsGuiCounter0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        player2WinsGuiCounter0.setStrokeWidth(0.0);
        player2WinsGuiCounter0.setText("player2wins");
        player2WinsGuiCounter0.setFont(new Font(14.0));
        setTop(GameInfoGridPane);

        gridPane.getColumnConstraints().add(columnConstraints);
        gridPane.getColumnConstraints().add(columnConstraints0);
        gridPane.getColumnConstraints().add(columnConstraints1);
        gridPane.getRowConstraints().add(rowConstraints);
        gridPane.getRowConstraints().add(rowConstraints0);
        gridPane.getRowConstraints().add(rowConstraints1);
        gridPane.getChildren().add(button0);
        gridPane.getChildren().add(button1);
        gridPane.getChildren().add(button2);
        gridPane.getChildren().add(button3);
        gridPane.getChildren().add(button4);
        gridPane.getChildren().add(button5);
        gridPane.getChildren().add(button6);
        gridPane.getChildren().add(button7);
        gridPane.getChildren().add(button8);
        GameInfoGridPane.getColumnConstraints().add(columnConstraints2);
        GameInfoGridPane.getColumnConstraints().add(columnConstraints3);
        GameInfoGridPane.getColumnConstraints().add(columnConstraints4);
        GameInfoGridPane.getRowConstraints().add(rowConstraints2);
        GamesCounterGridPane.getColumnConstraints().add(columnConstraints5);
        GamesCounterGridPane.getColumnConstraints().add(columnConstraints6);
        GamesCounterGridPane.getRowConstraints().add(rowConstraints3);
        GamesCounterGridPane.getChildren().add(gamesCounterField);
        GamesCounterGridPane.getChildren().add(gamesGuiCounter);
        GameInfoGridPane.getChildren().add(GamesCounterGridPane);
        Player1WinsGridPan.getColumnConstraints().add(columnConstraints7);
        Player1WinsGridPan.getColumnConstraints().add(columnConstraints8);
        Player1WinsGridPan.getRowConstraints().add(rowConstraints4);
        Player1WinsGridPan.getChildren().add(player1Field);
        Player1WinsGridPan.getChildren().add(player1WinsGuiCounter);
        GameInfoGridPane.getChildren().add(Player1WinsGridPan);
        Player2WinsGridPan.getColumnConstraints().add(columnConstraints9);
        Player2WinsGridPan.getColumnConstraints().add(columnConstraints10);
        Player2WinsGridPan.getRowConstraints().add(rowConstraints5);
        Player2WinsGridPan.getChildren().add(player2Field);
        Player2WinsGridPan.getChildren().add(player2WinsGuiCounter0);
        GameInfoGridPane.getChildren().add(Player2WinsGridPan);

    }
}
