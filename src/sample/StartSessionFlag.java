package sample;

import java.io.Serializable;

public class StartSessionFlag implements Serializable {
    private boolean startSessionFlag;

    public StartSessionFlag() {
        System.out.println("start flag constructor");
        this.startSessionFlag = true;
    }


    public boolean isStartSessionFlag() {
        return startSessionFlag;
    }

    public void setStartSessionFlag(boolean startSessionFlag) {
        this.startSessionFlag = startSessionFlag;
    }
}
