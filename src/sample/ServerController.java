package sample;

import java.io.*;
import java.net.Socket;
import java.util.Vector;

public class ServerController  extends Thread{

    ObjectInputStream player1ObjectInputStream;
    ObjectOutputStream player1ObjectOutputStream;
    ObjectInputStream player2ObjectInputStream;
    ObjectOutputStream player2ObjectOutputStream;
    XoGameNetworkData xoGameNetworkData;
    Socket player1Socket;
    Socket player2Socket;
    boolean player1Turn;
    boolean player2Turn;

    private Vector<ServerController> clientsVector =
            new Vector<ServerController>();
    public ServerController(Socket player1Socket , Socket player2Socket)
    {
        this.player1Socket = player1Socket;
        this.player2Socket = player2Socket;
        // da bygeb el socket elly da5el 3la el server mmkn ast5dmha en 2 yl3bo m3 b3d bs hzbotha ama a5ls el peer to peer
        System.out.println(this.player1Socket.getRemoteSocketAddress().toString().replace("/",""));
        System.out.println(this.player2Socket.getRemoteSocketAddress().toString().replace("/",""));

        try {
            //ps = new PrintStream(player1Socket.getOutputStream());
            //System.out.println("before o object");
            this.player1ObjectOutputStream = new ObjectOutputStream(player1Socket.getOutputStream());
            this.player2ObjectOutputStream = new ObjectOutputStream(player2Socket.getOutputStream());
            this.player1ObjectInputStream = new ObjectInputStream(player1Socket.getInputStream());
            this.player2ObjectInputStream = new ObjectInputStream(player2Socket.getInputStream());
      //      this.startSessionFlag = new StartSessionFlag();
            xoGameNetworkData = new XoGameNetworkData();
            this.player1Turn = true;
            this.player2Turn = false;
            //System.out.println("after o object");
        } catch (IOException e) {
            e.printStackTrace();
        }


        clientsVector.add(this);
        start();
    }
    public void run()
    {
        if(xoGameNetworkData.getStartSessionFlag()){

            try {
                System.out.println("set player 1 start true");
                player1ObjectOutputStream.writeObject(xoGameNetworkData);
                xoGameNetworkData.setCanPlay(false);
                player2ObjectOutputStream.writeObject(xoGameNetworkData);
                System.out.println("set player 2 start true");

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        while(true)
        {
            //String str = null;

            try {



                if(player1Turn){
                    xoGameNetworkData = (XoGameNetworkData) player1ObjectInputStream.readObject();
                    xoGameNetworkData.setStartSessionFlag(false);

                    System.out.println("player1Turn -> true "+xoGameNetworkData.getIndex());
                   // xoGameNetworkData.setVirtualSocketFromServer(this.player1Socket);
                    player1Turn = false;
                    player2Turn = true;
                    xoGameNetworkData.setPlayer1Turn(player1Turn);
                    xoGameNetworkData.setPlayer2Turn(player2Turn);
                    sendPlay(xoGameNetworkData);
//                    System.out.println("player1 turn");
                    continue;

                }
                if (player2Turn){
                    xoGameNetworkData = (XoGameNetworkData) player2ObjectInputStream.readObject();
                    xoGameNetworkData.setStartSessionFlag(false);
                    System.out.println("player2Turn -> true "+xoGameNetworkData.getIndex());
                    player1Turn = true;
                    player2Turn = false;
                    xoGameNetworkData.setPlayer1Turn(player1Turn);
                    xoGameNetworkData.setPlayer2Turn(player2Turn);
                    sendPlay(xoGameNetworkData);
//                    System.out.println("player2 turn");

                }

            } catch (IOException | ClassNotFoundException e) {
                break;
                //e.printStackTrace();
            }

        }
        try {
            player1ObjectInputStream.close();
            player1ObjectOutputStream.close();
            player2ObjectInputStream.close();
            player2ObjectOutputStream.close();
            player1Socket.close();
            player2Socket.close();

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
    void sendPlay(XoGameNetworkData xoGameNetworkData)
    {
        for(ServerController ch : clientsVector)
        {
            try {
                if(player1Turn == false){
                    xoGameNetworkData.setCanPlay(false);
                    ch.player1ObjectOutputStream.writeObject(xoGameNetworkData);
                    xoGameNetworkData.setCanPlay(true);
                    ch.player2ObjectOutputStream.writeObject(xoGameNetworkData);
                }
                if(player1Turn == true){
                    xoGameNetworkData.setCanPlay(true);
                    ch.player1ObjectOutputStream.writeObject(xoGameNetworkData);
                    xoGameNetworkData.setCanPlay(false);
                    ch.player2ObjectOutputStream.writeObject(xoGameNetworkData);
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}
