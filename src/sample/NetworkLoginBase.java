package sample;

import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.effect.Reflection;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;

public class NetworkLoginBase extends BorderPane {

    protected final BorderPane WholeGreatPane;
    protected final Pane pane;
    protected final Pane pane0;
    protected final Text text;
    protected final Button networkLoginBtn;
    protected final Reflection reflection;
    protected final Text text0;
    protected final Text text1;
    protected final Label label;
    protected final Label label0;
    protected final TextField nameText;
    protected final TextField ipText;

    public NetworkLoginBase() {

        WholeGreatPane = new BorderPane();
        pane = new Pane();
        pane0 = new Pane();
        text = new Text();
        networkLoginBtn = new Button();
        reflection = new Reflection();
        text0 = new Text();
        text1 = new Text();
        label = new Label();
        label0 = new Label();
        nameText = new TextField();
        ipText = new TextField();


        pane.setMaxHeight(USE_PREF_SIZE);
        pane.setMaxWidth(USE_PREF_SIZE);
        pane.setMinHeight(USE_PREF_SIZE);
        pane.setMinWidth(USE_PREF_SIZE);
        pane.setPrefHeight(553.0);
        pane.setPrefWidth(469.0);
        pane.setStyle("-fx-background-color: #080d21;");

        pane0.setLayoutX(51.0);
        pane0.setLayoutY(43.0);
        pane0.setPrefHeight(473.0);
        pane0.setPrefWidth(371.0);
        pane0.setStyle("-fx-border-color: #6dc4eb; -fx-border-width: 4px; -fx-border-style: dashed;");

        text.setFill(javafx.scene.paint.Color.valueOf("#95a8f4"));
        text.setLayoutX(94.0);
        text.setLayoutY(81.0);
        text.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text.setStrokeWidth(0.0);
        text.setText("Tic");
        text.setWrappingWidth(47.13671875);
        text.setFont(new Font("System Bold Italic", 34.0));

        networkLoginBtn.setLayoutX(205.0);
        networkLoginBtn.setLayoutY(356.0);
        networkLoginBtn.setMnemonicParsing(false);
        networkLoginBtn.setPrefHeight(43.0);
        networkLoginBtn.setPrefWidth(97.0);
        networkLoginBtn.setStyle("-fx-background-color: #312263; -fx-border-color: #e29cd8; -fx-border-style: solid; -fx-border-width: 4px;");
        networkLoginBtn.getStyleClass().add("btn");
        networkLoginBtn.setText("OK");
        networkLoginBtn.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        networkLoginBtn.setFont(new Font("Tw Cen MT Condensed Extra Bold", 27.0));
        networkLoginBtn.setCursor(Cursor.HAND);

        networkLoginBtn.setEffect(reflection);

        text0.setFill(javafx.scene.paint.Color.valueOf("#29f5d0"));
        text0.setLayoutX(153.0);
        text0.setLayoutY(81.0);
        text0.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text0.setStrokeWidth(0.0);
        text0.setText("Tac");
        text0.setWrappingWidth(60.13671875);
        text0.setFont(new Font("System Bold Italic", 34.0));

        text1.setFill(javafx.scene.paint.Color.valueOf("#f0869c"));
        text1.setLayoutX(219.0);
        text1.setLayoutY(82.0);
        text1.setStrokeType(javafx.scene.shape.StrokeType.OUTSIDE);
        text1.setStrokeWidth(0.0);
        text1.setText("Toe");
        text1.setWrappingWidth(60.13671875);
        text1.setFont(new Font("System Bold Italic", 34.0));

        label.setLayoutX(58.0);
        label.setLayoutY(126.0);
        label.setPrefHeight(23.0);
        label.setPrefWidth(120.0);
        label.setText("IGN");
        label.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        label.setFont(new Font("System Bold", 26.0));

        label0.setLayoutX(54.0);
        label0.setLayoutY(222.0);
        label0.setPrefHeight(23.0);
        label0.setPrefWidth(120.0);
        label0.setText("S/IP");
        label0.setTextFill(javafx.scene.paint.Color.valueOf("#f591ca"));
        label0.setFont(new Font("System Bold", 26.0));

        nameText.setLayoutX(58.0);
        nameText.setLayoutY(168.0);
        nameText.setPrefHeight(39.0);
        nameText.setPrefWidth(277.0);
        nameText.setStyle("-fx-background-color: #fff;");

        ipText.setLayoutX(58.0);
        ipText.setLayoutY(264.0);
        ipText.setPrefHeight(39.0);
        ipText.setPrefWidth(277.0);
        ipText.setStyle("-fx-background-color: #fff;");
        WholeGreatPane.setCenter(pane);
        setCenter(WholeGreatPane);

        pane0.getChildren().add(text);
        pane0.getChildren().add(networkLoginBtn);
        pane0.getChildren().add(text0);
        pane0.getChildren().add(text1);
        pane0.getChildren().add(label);
        pane0.getChildren().add(label0);
        pane0.getChildren().add(nameText);
        pane0.getChildren().add(ipText);
        pane.getChildren().add(pane0);

    }
}
