package sample;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XoGameBackEnd {
    private HumanPlayer humanPlayer1;
    private HumanPlayer humanPlayer2;
    private int player1Wins;
    private int player2Wins;
    private int gamesCounter;
    private AI aiPlayer;
    private int aiIndexPlay;
    private boolean aiOn;
    private boolean isOnline;
    private boolean drawFlag;
    private HumanPlayer.Type[] gameArray;
    private List<Integer> recordingArray;
    private NetworkConnection networkConnection;
    private ObjectOutputStream objectOutputStream ;
    private ObjectInputStream objectInputStream;


         XoGameBackEnd(){
         
        /*
        * create human player and fill gameArray with dummy token Z
        *
        * */
        this.humanPlayer1 = new HumanPlayer(HumanPlayer.Type.X);
        this.humanPlayer2 = new HumanPlayer(HumanPlayer.Type.O);
        this.aiPlayer = new AI(HumanPlayer.Type.O);
        this.aiOn = this.aiPlayer.isAiOn();
        this.gameArray = new HumanPlayer.Type[9];
        this.recordingArray = new ArrayList<>(9);
        Arrays.fill(this.gameArray, HumanPlayer.Type.Z);
        
       // viewButtonsAction();
    }

         XoGameBackEnd(HumanPlayer player1,AI ai){
        this.humanPlayer1 = player1;
        this.aiPlayer = ai;
        this.gameArray = new HumanPlayer.Type[9];
        Arrays.fill(this.gameArray, HumanPlayer.Type.Z);
    }
         XoGameBackEnd(HumanPlayer player1, HumanPlayer player2) {
        this.humanPlayer1 = player1;
        this.humanPlayer2 = player2;
        this.gameArray = new HumanPlayer.Type[9];
        Arrays.fill(this.gameArray, HumanPlayer.Type.Z);
    }

         boolean play(int tokenPos,boolean player1Turn){

        /*
        * game logic
        *
        * */

        humanPlayer1.setTurn(player1Turn);
        humanPlayer2.setTurn(!player1Turn);
        aiPlayer.setTurn(!player1Turn);

            if(humanPlayer1.isTurn()){
                this.gameArray[tokenPos] =  humanPlayer1.getGameToken();
                this.recordingArray.add(tokenPos);
                System.out.println("player1 ->"+"Pos ->"+tokenPos+" Token ->"+this.gameArray[tokenPos]);

                if(winCheck(this.gameArray)){

                    humanPlayer1.setWinCount(humanPlayer1.getWinCount()+1);
                    humanPlayer2.setLoseCount(humanPlayer2.getLoseCount()+1);
                    this.player1Wins = humanPlayer1.getWinCount();
                    System.out.println("Player one Won");
                    System.out.println(humanPlayer1.getWinCount());
                    gameCounterUpdate();

                    return true;
                }

                this.humanPlayer1.setTurn(false);
            }

            if(humanPlayer2.isTurn() || aiPlayer.isTurn()){
                System.out.println("inside player 2 backend");

                this.gameArray[tokenPos] = humanPlayer2.getGameToken();
                this.recordingArray.add(tokenPos);
                    if (winCheck(this.gameArray)){
                    humanPlayer2.setWinCount(humanPlayer2.getWinCount()+1);
                    humanPlayer1.setLoseCount(humanPlayer1.getLoseCount()+1);
                    this.player2Wins = humanPlayer2.getWinCount();
                    gameCounterUpdate();
                    System.out.println("player Two Won");
                    System.out.println(humanPlayer2.getWinCount());
                    return true;
                }

                System.out.println("player2 ->"+ "Pos ->"+tokenPos+" Token ->"+this.gameArray[tokenPos]);
                if(aiPlayer.isAiOn()){
                    this.aiPlayer.setTurn(true);
                }else{
                    this.humanPlayer2.setTurn(true);
                }
            }

            if(gameEnd(this.gameArray)){
                gameCounterUpdate();
                System.out.println("Draw");
                drawFlag = true;
                return true;
            }
            return false;
    }

    private boolean gameEnd(HumanPlayer.Type[] array){

        /*
        * draw logic
        *
        * */
        for (int i=0 ; i<array.length ; i++) {
            if (array[i] == HumanPlayer.Type.Z) {
                return false;
            }
        }
        return true;
    }


    private boolean winCheck(HumanPlayer.Type[] array){
        /*
        * win check logic
        *
        * */
        if((array[0] == HumanPlayer.Type.O || array[0] == HumanPlayer.Type.X) && (array[0]==array[1] && array[0]==array[2])){
            return true;
        }
        if((array[0] == HumanPlayer.Type.O || array[0] == HumanPlayer.Type.X) && (array[0]==array[4] && array[0]==array[8] )){
            return true;
        }
        if((array[0] == HumanPlayer.Type.O || array[0] == HumanPlayer.Type.X) && (array[0]==array[3] && array[0]==array[6]) ){
            return true;
        }
        if((array[2] == HumanPlayer.Type.O || array[2] == HumanPlayer.Type.X) && (array[2]==array[4] && array[2]==array[6])){
            return true;
        }
        if((array[2] == HumanPlayer.Type.O || array[2] == HumanPlayer.Type.X) && (array[2]==array[5] && array[2]==array[8])){
            return true;
        }
        if((array[6] == HumanPlayer.Type.O || array[6] == HumanPlayer.Type.X) &&  (array[6]==array[7] && array[6]==array[8])){
            return true;
        }
        if((array[3] == HumanPlayer.Type.O || array[3] == HumanPlayer.Type.X) &&  (array[3]==array[4] && array[3]==array[5])){
            return true;
        }

        if((array[1] == HumanPlayer.Type.O || array[1] == HumanPlayer.Type.X) &&  (array[1]==array[4] && array[1]==array[7])){
            return true;
        }

        return false;
    }


         void restartXoGame(){
        System.out.println("BackEnd Game Restarted");
        Arrays.fill(this.gameArray, HumanPlayer.Type.Z);
        this.drawFlag = false;
        this.recordingArray.clear();
    }


    private void gameCounterUpdate(){
        humanPlayer1.setNumberOfGamesPlayed(humanPlayer1.getNumberOfGamesPlayed()+1);
        this.gamesCounter=humanPlayer1.getNumberOfGamesPlayed();

    }

    public void setAiIsOn(){
              this.aiPlayer.setAiOn(true);
        this.aiOn = this.aiPlayer.isAiOn();

    }

    public void connectOnline(String serverIp) {

        System.out.println(serverIp);
        this.networkConnection = new NetworkConnection(serverIp, 5005);
        this.objectInputStream = this.networkConnection.getObjectInputStream();
        this.objectOutputStream = this.networkConnection.getObjectOutputStream();
        this.humanPlayer1.setActive(true);
        this.isOnline = this.humanPlayer1.isActive();

    }

    public int aiPlay(){

             return aiPlayer.aiPlay(gameArray);
    }

    public  int easyAi(){
             return aiPlayer.getEasyAi(gameArray);
    }

    public int normalAi(){
             return aiPlayer.getNormalAi(gameArray);
    }

    public int hardAi(){
             return aiPlayer.getHardAi(gameArray);
    }


         int getPlayer1Wins() {
        return player1Wins;
    }

         int getPlayer2Wins() {
        return player2Wins;
    }

         int getGamesCounter() {
        return gamesCounter;
    }

    public boolean isAiOn() {
        return aiOn;
    }

    public HumanPlayer.Type[] getGameArray() {
        return gameArray;
    }

    public List<Integer> getRecordingArray() {
        return recordingArray;
    }

    public int getAiIndexPlay() {
        return aiIndexPlay;
    }

    public ObjectOutputStream getObjectOutputStream() {
        return objectOutputStream;
    }

    public ObjectInputStream getObjectInputStream() {
        return objectInputStream;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public boolean isDrawFlag() {
        return drawFlag;
    }
}
