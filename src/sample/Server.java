package sample;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    ServerSocket serverSocket;
    public Server()
    {
        try {
            serverSocket = new ServerSocket(5005);
        } catch (IOException e) {
            e.printStackTrace();
        }
        while(true)
        {
            Socket player1Socket = null;
            Socket player2Socket = null;
            try {
                player1Socket = serverSocket.accept();
                player2Socket = serverSocket.accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            new ServerController(player1Socket,player2Socket);
        }
    }
    public static void main(String[] args)
    {
        System.out.println("Server Is On");
        new Server();
    }

}
